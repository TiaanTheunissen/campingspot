<?php

Route::get('/', function () {
    return view('welcome');
});

/*
 * Application Routes
 */

/*
 * Newsletter subscription routes
 */
require_once "app/newsletter.php";
