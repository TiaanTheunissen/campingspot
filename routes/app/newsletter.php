<?php

Route::namespace('Newsletter')->group(function () {
    Route::name('newsletter.')->group(function (){
        Route::get('subscribe/newsletter', 'NewsletterController@create')->name('new_subscriber');
        Route::post('subscribe/newsletter', 'NewsletterController@store')->name('save_subscriber');
    });
});