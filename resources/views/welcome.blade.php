<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <title>Camping Site</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #ffffff;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
                background-color: #ffffffcc;
                padding: 25px;
                border-radius: 12px;
            }

            .links > a {
                color: #ffffff;
                padding: 20px 25px;
                background-color: #7ebe98;
                font-size: 16px;
                font-weight: bold;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .button-send {
                color: #ffffff;
                padding: 17px 25px;
                background-color: #7ebe98;
                border-color: #7ebe98;
                border-radius: 0px;
                font-size: 16px;
                font-weight: bold;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                vertical-align: center;
            }

            .button-send:hover{
                color: #ffffff;
                background-color: #7ebe98;
                border-color: #7ebe98;
                font-size: 16px;
                font-weight: bold;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .newsletter{
                margin-top: 40px;
                background-color: #ffffffcc;
                padding: 25px;
                border-radius: 12px;
                font-size: 16px;
                color: #898989;
                font-weight: bold;
                text-align: left;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="background-image: url('/images/bg-default.jpg'); background-size: cover">

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <img src="{!! asset('/images/logo.png') !!}" alt="">
                </div>

                <div class="links">
                    <a href="https://www.facebook.com/campingspots/"><i class="far fa-thumbs-up"></i> Facebook Community</a>
                    <a href="https://www.facebook.com/groups/274929699707093"><i class="fas fa-thumbs-up"></i> Facebook Group</a>
                </div>

                <div class="newsletter">
                    <h4>Newsletter</h4>
                    <hr>
                    {!! Form::open(['method' => 'post', 'route' => 'newsletter.save_subscriber']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                                {!! Form::input('text', 'first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                                @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                                {!! Form::input('text', 'last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                                @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group @if ($errors->has('email')) has-error @endif">
                                {!! Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder' => 'Email Address']) !!}
                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="links">
                                <button class="btn btn-primary button-send"><i class="fa fa-check"></i> Subscribe to newsletter</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        @include('sweet::alert')
    </body>
</html>
