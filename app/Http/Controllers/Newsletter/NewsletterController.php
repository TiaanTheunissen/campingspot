<?php

namespace App\Http\Controllers\Newsletter;

use App\Http\Requests\Newsletter\NewsletterRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sendinblue\Mailin;

class NewsletterController extends Controller
{
    public function create()
    {
        return view('welcome');
    }

    public function store(NewsletterRequest $request)
    {
        $mailing = new Mailin('https://api.sendinblue.com/v2.0', env('SENDINBLUE_KEY'));
        $result = $mailing->create_update_user([
            'email' => $request['email'],
            'attributes' => [
                'FIRSTNAME' => $request['first_name'],
                'LAST_NAME' => $request['last_name'],
            ],
            'listid' => ['4']
        ]);

        if ($result['code'] === 'failure'){
            alert()->error('Something went wrong, please try again later', 'Unable to save');
        }else{
            alert()->success('Aweomse!, you ar subscribed to our newsletter', 'Subscribed');
        }
        return back();
    }
}
